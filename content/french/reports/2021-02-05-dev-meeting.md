---
title: dev meeting report
description: 05/02/2021
date: 2021-02-05
draft: true
lang: fr
#bg_image: "images/backgrounds/library.jpg"
#image: "images/blog/"
author: [ "vit", "Hugo Trentesaux" ]
categories: ["report"]
tags: ["ux", "api"]
type: "post"
---

# Gchange/Cesium

 * Proposition de fusionner les projets Pod Cesium+ et Pod Ğchange en un seul projet Pod Ğchange. Afin de faciliter le développement et la maintenance.
 * Le client Ğecko utilise actuellement les données des Pods Cesium+ et compte aussi utiliser les données des Pods Ğchange.
 * La transformation de Ğchange en application (que je sais déjà en cours) pourrait commencer la communication à destination des utilisateurs (site web en lecture seule à telle date, application Ğchange téléchargeable sur tel et tel site, etc).

# API GVA

 * Dans l’api GVA, il manque ce qui concerne les données de la Toile de Confiance présentes en piscine. Mais cela nécessite un gros chantier dans le code de Duniter. En attendant, il est possible de faire un mock délivrant des données de test, afin que les clients puissent commencer à développer des fonctionnalités sur la Toile de Confiance.

# Wotwizard

 * @Poka va installer un autre serveur pour soulager celui de cgeek. Ceci afin d’activer l’api graphQL. Les contributeurs web pourront ainsi développer facilement des clients web pour Wotwizard.
 * Wotwizard va utiliser l’api GVA. Mais pour cela il faut que GVA publie des fonctionnalités sur la Toile de Confiance. Un mock dans GVA doit être fait à cette fin.

# White Paper et Site Web

 * @HugoTrentesaux va travailler sur mille projets. Mais la priorité semble être le White Paper et le site Web, en attendant que l’api GVA nécessite son aide pour les fonctionnalités de la Wot.

# Tikka

 * Tikka se destine à être un client riche (pas lourd hein, riche !). Principalement en ajoutant des fonctionnalités pour les commerçants, idéalement pour les TPE des commerçants. Il sera aussi ajouté des graphiques grâce à des binding vers des bibliothèques en Rust créés par @tuxmain.

# Ğecko

 * Ğecko permet de se créer des portefeuilles de paiement rapidement. Des facilités de paiement des annonces Ğchange sont prévues grâce à l’api des pods Ğchange. Il utilise aussi les Pod Cesium+ et il sera donc cohérent d’essayer de fusionner les deux projets de Pods.

# Langage Julia

 * Comme ci ce n’était déjà pas assez le bordel dans la multiplication des langages, @HugoTrentesaux nous a parlé de Julia. Un langage compilé qui semble plus performant que Rust sur certains calculs et plus simple à programmer pour un scientifique/matheux qui n’a pas une formation de développeur. On a donc un outil de plus pour les calculs gourmands !

# Portefeuille HD (hierarchical deterministic) [ajouté par Spencer]

 * Style bip32 : un portefeuille HD permet l’utilisateur de gérer plusieurs clé-publiques avec une seule « HD seed » privée, et aussi de partager l’info nécessaire, sélectivement, pour que ses clients puissent générer les mêmes clé-publiques pour les paiements à venir.
 * Faire une RFC pour reproduire ce protocole avec Scrypt.

*Rédigé par @vit. N’hésitez pas à ajouter des choses oubliées si besoin.*
