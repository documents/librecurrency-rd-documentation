---
title: "Contact"
draft: false
# page title background image
bg_image: "images/backgrounds/world.jpg"
# meta description
description : ""
---

Plusieurs moyens sont à votre disposition pour nous contacter :

### Dans nos salons de discussion

https://chat.axiom-team.fr/

### Sur le forum

https://forum.monnaie-libre.fr/

### Par courriel

