
---

This is a static website made with [hugo](https://gohugo.io).

Learn more about GitLab Pages at https://pages.gitlab.io and the official
documentation https://docs.gitlab.com/ce/user/project/pages/.

---

## Structure

Almost all pages are in the [content/{language}](content/) subdirectories.

Some translations belong to the website and are in the [i18n](i18n/)
subdirectory, while some others belong to the theme (and are within it).

The theme(s) are in the [themes](themes/) subdirectory (and they should be git submodules).

(Alas) There are still some interdependencies between the themes and the hugo website and
its content :
 * the theme use the website images.
 * Some descriptions are in the theme while they could have been in the website,
   or vice versa.

## Languages feature - i18n support

When you choose a language in the menu bar, you switch content and should translate each page into all languages.

Fortunately our readers use to read multiple languages, and for the more dynamic
part of content, which are blog, events and training pages, the theme manage the
**lang** field present in [yaml
headers](https://cran.r-project.org/web/packages/ymlthis/vignettes/yaml-fieldguide.html).

Then if you want a page written in English, to be displayed in the "French"
website, just create a symlink from the French part, to this English page. Or
vice versa.

> $ cd content/french/blog/

> $ ln -s ../../english/blog/1970-01-01-my-first-article.md

## Develop and test this website locally

You should use a text editor which support [Markdown](https://en.wikipedia.org/wiki/Markdown)
syntaxes ([vim](https://www.vim.org/), etc.)

Then you should install [hugo](https://github.com/gohugoio/hugo/releases).

Then you should run:

> $ hugo server -D

To tell hugo to generate the static website, and make it run on your
[localhost](https://en.wikipedia.org/wiki/Localhost), with the draft pages
enabled.

Read more at Hugo's [documentation][].

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml).

### Preview your site

If you clone or download this project to your local computer and run `hugo server`,
your site can be accessed under `localhost:1313/hugo/`.

## GitLab User or Group Pages

To use this project as your user/group website, you will need one additional
step: just rename your project to `namespace.gitlab.io`, where `namespace` is
your `username` or `groupname`. This can be done by navigating to your
project's **Settings**.

You'll need to configure your site too: change this line
in your `config.toml`, from `"https://pages.gitlab.io/hugo/"` to `baseurl = "https://namespace.gitlab.io"`.
Proceed equally if you are using a [custom domain][post]: `baseurl = "http(s)://example.com"`.

Read more about [user/group Pages][userpages] and [project Pages][projpages].

## Did you fork this project?

If you forked this project for your own use, please go to your project's
**Settings** and remove the forking relationship, which won't be necessary
unless you want to contribute back to the upstream project.

[ci]: https://about.gitlab.com/gitlab-ci/
[hugo]: https://gohugo.io
[install]: https://gohugo.io/overview/installing/
[documentation]: https://gohugo.io/overview/introduction/
[userpages]: http://doc.gitlab.com/ee/pages/README.html#user-or-group-pages
[projpages]: http://doc.gitlab.com/ee/pages/README.html#project-pages
[post]: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/#custom-domains



