---
title: "Partenaires"
draft: false
# page title background image
bg_image: "images/backgrounds/world.jpg"
# meta description
description : "Entreprises, associations et administrations promouvant ou utilisant des monnaies libres. [Contactez-nous](/fr/contact) pour devenir partenaire&nbsp;!"
---
