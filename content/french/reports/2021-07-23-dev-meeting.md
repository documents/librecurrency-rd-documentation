---
title: dev meeting report
description: 23/07/2021
date: 2021-07-23
draft: true
lang: fr
#bg_image: "images/backgrounds/library.jpg"
#image: "images/blog/"
categories: ["report"]
tags: ["blockchain", "identity"]
type: "post"
---


Ordre du jour:
 * Gestion des identités et de la toile de confiance ?
 * Étude de la migration de la Ĝ1 vers une blockchain [substrate](https://substrate.dev/)

Elois fera beaucoup de visio one to one avec les principaux contributeurs, en plus des réunions générales comme celle-ci.

Vue l'ampleur du chantier de migration Duniter -> substrate, on ne fera certainement pas de modifications de règle de wot avant cette migration, cela ferait trop de boulot. On pourra reparler des règles de la wot plus tard.

## aperçu de nos forces de travail

Eloïs appuyé par tuxmain maîtrisent les codes des noeuds de la blockchain (Duniter en JavaScript/TypeScript + Rust)

Kimamila continue les développement en mode progressive web app pour les clients cesium v2, comme pour V1.

Moul et vit sont toujours sur python, ils réfléchissent pour évaluer leurs contributions.

## Indexation, DHT

Elois propose de faire un stockage libre basé sur une DHT kademlia, le même binaire que substrate, donc utilisable avec la même API RPC.
Il est en réflexion sur la conception, anti spam ect ...
Mais il faut en plus un serveur d'indexation avec des hash pour requeter la DHT.

JBAR:

http://www.openudc.org:11371/pks/
http://www.openudc.org:11371/pks/lookup?search=cedric
http://www.openudc.org:11371/pks/lookup?search=jbar

Au sujet de l'indexation, OpenPGP.

Pour l'indexation : IPFS, IPNS, LibP2P/Kademlia, module off-chain intégré au nœud (il existe des moteurs full-text Rust: sonic, toshi...)

## Gecko, API clients

Gecko : binding Rust ou JS ? (pour l'API)
Rust -> rapidité
JS -> permet le web
Dart -> en développement...

## Archivage des comptes-rendus sur le Git

Jbar commence un dépôt pour archiver les compte-rendus de réunions sur le GitLab Duniter.
https://git.duniter.org/documents/librecurrency-rd-documentation/

GitFlow vs GitLab workflow (master stable vs master dev)

## Timestamps de la visio

```
timestamps visio dev
--- première partie ---
00:00 introduction par elois de la migration substrate
02:05 questionnement sur la migration de la ğ1 / création ğ2
03:15 intervention jbar
03:50 changements déjà prévus (mempool, consensus, dht)
05:02 discussion clients (césium v2, ǧecko, tikka)
06:20 discussion Rust/substrate, apprentissage
07:33 intervention Moul
08:20 discussion de timeline générale
09:40 intervention Poka/Kimamila pods Cesium+, indexation des blocs
10:33 elois rappel sur la DHT, hashs en blockchain, serveurs d'indexation
13:20 (le ventilo de elois s'excite)
13:56 intervention jbar http://openudc.org:11371/pks/lookup?search=attilax
16:54 réponse elois/poka
19:05 crate Rust libp2p
20:12 intervention kimamila
22:16 discussion IPFS/service base de données (kimamila/poka)
24:47 tuxmain itégration substrate
25:50 elois client substrate / wallet
28:50 discussions méthodologiques jbar/poka/elois
30:17 (le ventilo s'emballe encore)
31:41 comptes rendus sur dépôt
--- fin première partie ---
32:26 elois "posez vos questions"
33:04 poka discussion ğecko lib javascript / binding dart, UX mobile
41:11 elois génération de la génération de la lib JS de l'API RPC
43:08 dépôt compte-rendus
--- la suite de la discussion s'étiole, j'arrête les timestamps ---
```

## liens pour études

https://www.notamonadtutorial.com/sonic-a-minimalist-alternative-to-elasticsearch-written-in-rust/

https://rs-ipfs.github.io/offchain-ipfs-manual/

https://medium.com/equilibriumco/tech-preview-2-rust-ipfs-substrate-848b8a1afb26

---

Smart contracts : pas encore prêts pour Substrate mais Elois pourra voir quand ça sera bon.


Notifications par plusieurs moyens pour ramener les gens désintéressés, alerter d'événements futurs (expiration, etc.)

Galuel -> Appuie sur mettre plutôt la priorité sur la migration, on changera la WoT quand tout marchera.

Aller sur les exchanges.

Elois doit préciser les differents niveau/zones de données : on-chain, off-chain, et DHT.

