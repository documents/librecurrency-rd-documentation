---
title: "About Us"
draft: false
# page title background image
bg_image: "images/banner/banner-1.jpg"
# about image
image: ""
# meta description
description : ""
---

![pugs puppies](/images/logo.png)

### The Axiom-Team association fulfills two missions:

* Help members promote free currency by providing users with tools to organize local events. We offer graphic designs (flyers, posters, stickers) and logistical advice in the form of documents available online.

* Support the developers of the Duniter ecosystem by providing organizational tools and funding. For this, we have two permanent funds intended to welcome donations in euros and ğ1.

### A dedicated team

Axiom Team members combined their energies to produce a diverse and cohesive vision of free currency. By bringing together free electron profiles in the service of a Free Currency, we hope to gradually spread the idea of ​​a smooth monetary paradigm shift, among all communities, human by human, freely.

### Axiom Team antennas

The association operates through regional branches in order to decentralize the organization of events.

Each regional branch has the insurance coverage provided by the Axiom-Team association.
It also has the support and mutual assistance of the free currency network, and support in administrative procedures if there is any.

To apply as an Axiom-Team branch, just send an email to contact@axiom-team.fr
Allow 2/3 days of response time.
