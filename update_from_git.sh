#!/bin/bash

# Call this script with the -D or --buildDrafts to build also content marked as draft.
# And put it on a cron to synchronize periodically

cd $(dirname "$0")

head="$(git rev-parse HEAD)"

git stash
git pull --rebase --recurse-submodules
git stash pop

[[ $(git rev-parse HEAD) == $head ]] || hugo "$@"
