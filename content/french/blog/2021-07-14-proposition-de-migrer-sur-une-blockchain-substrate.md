---
title: Proposition de migrer la Ğ1 sur une blockchain substrate
description: IMPORTANT
date: 2021-07-14
draft: false
lang: fr
#bg_image: "images/backgrounds/library.jpg"
#image: "images/blog/"
author: Elois
categories: ["study"]
tags: ["migration", "blockchain", "substrate"]
type: "post"
---

*Vidéo précédent ce post : https://tube.p2p.legal/videos/watch/0b54d898-fc5b-4443-83f2-bda45b4cc53f*

# IMPORTANT : Proposition de migrer la Ğ1 sur une blockchain substrate

Bonjour à tous,

Avant de répondre à ce post, merci de prendre le temps de tout lire afin de bien comprendre mon point de vue.

Mon objectif long-terme est de contribuer à la mise en place d'une monnaie libre sécurisée, scalable (capable de monter en charge à grande échelle), fiable et robuste (byzantin résistant) avec un paiement le plus rapide et fiable possible et de la gouvernance on-chain pour que la gestion de cette monnaie soit la plus horizontale possible.

Nous ne sommes que 3 000 membres et déjà de plus en plus de problèmes techniques se font sentir avec Duniter, la synchro de la blockchain est déjà devenue quasi impossible, plusieurs anciens membres forgeron (dont @tuxmain) ne le sont plus à cause de ça. On a régulièrement des problèmes de transactions qui ne passent pas lors des ğmarchés, on l'a vécu encore ce week-end à l'université d'été.
Et je ne parle pas des problèmes de synchro des mempool qui transforment l'entrée dans la toile en un véritable parcours du combattant.

Duniter est une PoC (Preuve de Concept), écrite entièrement de zéro, dans un langage pas adapté à la montée en charge, par un développeur qui n'avait jamais développé de blockchain auparavant, et qui a donc fait un bon nombre d'erreur de design, que j'aurais faites aussi à sa place, c'est normal.

Le but n'est pas de critiquer mais d'expliquer pourquoi Duniter n'est plus gérable aujourd’hui et pourquoi il vaut mieux migrer au plus vite vers une autre solution.

@cgeek lui-même n'arrivait plus à faire évoluer Duniter facilement, il se sentait « pris dans la colle » me disait-il en privé il y a déjà quelques années. La Ğ1 a déjà quatre ans et demi, et en quatre ans et demi je suis le seul autre développeur à avoir réussi à rentrer substantiellement dans le code de Duniter, parce que j'avais beaucoup de temps et une motivation et béton armé.

Mais depuis des années je galère à essayer d'améliorer Duniter, et je constate depuis un certains temps que c'est infiniment plus de boulot d'essayer de retaper une vielle maison plutôt que de tout raser et en construire ure nouvelle.

Migrer entièrement Duniter en Rust me prendrait beaucoup trop de temps, et c'est seulement après migration que j'aurais pu apporter les nombreuses améliorations de protocole que je souhaite apporter depuis des années.
Je viens d'y passer un an à plein temps, et cette année à plein temps m'a permis de me prendre en pleine face toutes les limitations dues à l'état actuel de Duniter.

En parallèle de ça, cela fait longtemps que je me disais que ce serait bien que l'on trouve un framework qui puisse gérer pour nous certaines briques pour que l'on ai moins de choses à maintenir, pour que l'on puisse se concentrer sur notre cœur de métier : le dividende universel et la toile de confiance. Mais tous les frameworks que je trouvais ne permettais pas suffisamment de personnalisation pour qu'on puisse y implémenter notre cœur de métier, ou/et n'était pas mature ou/et pas suffisamment maintenu et utilisé.

Il y a trois mois, @nanocryk (avec qui je suis toujours en contact régulier depuis quatre ans), m'a fait découvrir le framework [substrate](https://substrate.dev/). J'étais alors très sceptique, et convaincu que comme tous les autres frameworks ça n'irait pas pour nos besoins, que ce serait pas assez customisable, etc. J'ai creusé quand même, et plus je creusais plus j'étais surpris. C'est la première fois que je tombe sur un framework blockchain aussi customisable, qui ne limite pas ses utilisateurs avec des hypothèses trop fortes, et qui fournit un cadre d'abstraction aussi propre pour que l'on puisse se concentrer rapidement et efficacement sur notre cœur de métier.

La question a naturellement émergé dans mon esprit : « Serait-il possible techniquement de migrer la Ğ1 sur une blockchain substrate, et si oui, est-ce que ce serait compliqué ?».

J'ai donc décidé de creuser à fond la question de la faisabilité technique, me disant que ça ne servait à rien d'en parler ni de proposer quoi que ce soit tant que je n'étais pas certain de la faisabilité.

J'en ai conclu il y a deux semaines que oui c'est possible et je vois désormais exactement comment faire. Entre-temps j'ai continué à creuser, et j'ai même codé une petite PoC en un week-end ! non seulement c'est possible, mais en plus c'est facile !

Si j'étais encore au chômage, je pourrais recoder toute la logique métier nécessaire à la Ğ1 en seulement quelques semaines, comme j'ai repris un boulot à plein-temps, ça va me prendre 5-6 week-ends.

Une fois que j'aurais codé ça, on va lancer une monnaie libre de test (avec un taux `c` très élevé) avec trois validateurs (=membres forgerons) : @tuxmain, @poka et moi.

Grâce à cette monnaie de test, on va pouvoir développer ce qu'il faut côté client, c'est en fait les développements côté client qui vont prendre le plus de temps, ça va être le gros boulot de cet automne. Dès qu'on aura au moins un client près, on pourra déjà essayer de lancer une blockchain reprenant l'état de la Ğtest dans son bloc genesis. 

Voici une ébauche d'une roadmap de migration possible, ce n'est qu'en proposition :

1. Développement d'un runtime substrate qui gère le DU et la toile de confiance (déjà commencé) 
2. lancement d'une monnaie de test
3. Développement d'au moins un client pour utiliser cette monnaie de test (Ğecko), si des dev d'autres clients veulent nous rejoindre ce serait génial, il y a des bibliothèques JS et Python très complètes pour faire un client d'une blockchain substrate.
4. Développement d'un programme qui lit l'état d'une DB Duniter et produit une chain-spec pour substrate
5. Lancement d'une 2ᵉ monnaie de test, qui reprend l'état de la ğ1-test dans son genesis.
6. Tests intensifs de cette 2ᵉ monnaie de test
7. Organisation d'une «fausse migration» de la Ğ1, un « essai à blanc », avec un admin central qui peut reset le réseau si besoin.
8. Refaire des fausses migrations jusqu'à maîtrise parfaite du processus.
9. Réaliser une pré-migration publique, en communiquant cette fois-ci auprès de toute la communauté et en lançant un appel à utilisateurs volontaires pour tester  cette « fake Ğ1 », qui sera identique en tout point à la « nouvelle Ğ1 » à ceci près qu'un admin central pourra tuer le réseau. Avec bien sur gros warning rouge sur les clients indiquant que cette monnaie temporaire va disparaitre, ne vendez pas des vrais biens et services avec.
10. Poser une date pour la vraie migration de la Ğ1, qui sera techniquement identique en tout point à la fausse migration publique, à l'exception de l'admin central qui ne sera pas présent.

Si on est suffisamment nombreux là-dessus, sachant que @tuxmain et @poka vont m'aider à fond, je pense qu'on peut réaliser ces dix étapes en six mois.

Si cette proposition est acceptée, je me chargerai de la maintenance minimale de Duniter le temps de cette transition.

Je comprends tout à fait que cela implique de quasiment tout refaire côté client, et je comprends que certains aient le sentiment qu'il s'agirait de renier des années de travail, moi-même je vais « jeter à la poubelle » des milliers d'heures de travail sur cinq ans.

D'une part, on n'a pas fait tout ça pour rien, on a appris, on est monté en compétences techniquement. Sur tout ce que vous avez appris pendant ces années de contributions, beaucoup de choses vous seront très utile dans le nouvel environnement technique.
D'autre part, servir l'objectif est le plus important, si jeter l'existant permet de beaucoup mieux servir l'objectif et beaucoup plus vite, alors moralement on se doit de le faire. 
Enfin, il n'y a pas forcément besoin de tout jeter, certains outils pourraient rester avec assez peu de modifications (comme ğchange par exemple).

Développer avec substrate permet d'aller bien plus vite car plein de choses sont générées automatiquement à partir du code de notre protocole blockchain (grâce au système de typage évolué de Rust). Par exemple, substrate me génère automatiquement l'API RPC qui permet d'appeler les extrinsics que j'ai codé mais aussi de lire le storage que j'ai défini. Substrate gère également automatiquement les mempool et la couche réseau inter-nœud, je n'ai absolument rien à faire de ce côté !

Et même la logique métier du protocole lui-même, dans Duniter elle est répartie dans quatre endroits différents (génération d'un bloc, validation d'un bloc, application d'un bloc, synchronisation d'un bloc).
Dans substrate tout ça est remplacé par la notion d'exécution d'un bloc. L'exécution d'un bloc étant parfaitement déterministe à partir du code du runtime, pour le vérifié il suffit de l'exécuter.

En fait il y a plein de concepts de substrate long à expliquer qu'il faut étudier et comprendre pour comprendre à quel point ça permet d'aller beaucoup plus vite et avec un code beaucoup plus optimisé et sécurisé, je n'ai pas le temps de tout expliquer là, ça viendra au fur et à mesure dans les semaines et mois à venir, je ferai au moins une conférence pour expliquer une partie de tout ça (voir plusieurs).

Je comprends que ma proposition chamboule tout, mais si je propose de migrer la Ğ1 sur substrate c'est parce que je pense sincèrement que c'est ce que l'on a de mieux a faire pour fiabiliser et pérenniser la Ğ1.

Il ne m'a fallu que trois jours pour développer une blockchain substrate qui gère déjà le Dividende Universel et les identités (sans certifications pour le moment) :

Dépôt de la poc : https://git.duniter.org/nodes/rust/lc-core-substrate
Vidéo démo de la poc : https://drive.infomaniak.com/app/share/144117/c823a9fc-3d1b-4d34-9bfc-9f347cbd566a

Une évidence que je n'ai pas dit dans la vidéo : le client que j'utilise est réservé aux développeurs et testeurs avancés, c'est plus une sorte de postman amélioré, ce n'est évidemment pas destiné aux utilisateurs lambda !

Les prochains week-ends je vais implémenter les certifications, écrire plus de tests pour fiabiliser tout ça (oui j'en ai déjà écrit quelques-uns), et préparer ce qu'il faut pour lancer une première monnaie de test avec @tuxmain, @poka et tous ceux qui voudront :)

Côté client, il y a des bibliothèques officielles pour JavaScript, Python, Go, Rust, etc : 
https://substrate.dev/docs/en/knowledgebase/integrate/libraries

Une fois la monnaie de test lancée, on va travailler sur ğecko avec @poka (et peut-être @tuxmain s'il veut bien nous aider sur le binding Rust), pour avoir un client qui fonctionne avec cette nouvelle blockchain.

Je pense en profiter pour pousser quelques changements de protocole en même temps, j'en dirai plus dans d'autres sujets dans les semaines à venir :)

