---
title: "contribute financially "
lang: en
#date: 2019-07-06T15:27:17+06:00
draft: false
bg_image: "images/backgrounds/puzzle.jpg"
description : "Donate to Axiom-Team to support the libre currencies."
image: "images/donate/logo_g1_flare_512.png"
categories: ["about"]
tags: ["founding"]
type: "post"
---

You can contribute by donating in June (Ǧ1) to the Axiom Team account.
Public key:
[8CWuf4f1jYoVzHh4DEFpCyzZYC1pgz4t2wU8F2zKCthh](https://g1.duniter.fr/#/app/wot/8CWuf4f1jYoVzHh4DEFpCyzZYC1pgz4t2wU8hF2zK)

For a participation in euros (donation, membership, pot, etc.) you can go through our [membership page](https://axiom-team.fr/adherer), or by sending a check to: Axiom Team 14 rue Johannes Kepler 31500 Toulouse.
