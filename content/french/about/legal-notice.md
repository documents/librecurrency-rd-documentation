---
title: "Mentions Légales"
lang: fr
#date: 2019-07-06T15:27:17+06:00
draft: false
bg_image: "images/backgrounds/puzzle.jpg"
description : "Mentions et conditions d'utilisations"
image: "images/logo.png"
categories: ["about"]
tags: ["governance"]
type: "post"
---

Page à rédiger en markdown [sur un pad](https://pad.p2p.legal/).

Ou la proposer directement sur [notre dépot git](https://git.duniter.org/documents/librecurrency-rd-documentation/-/tree/master/content).

## Éditeur

[Statuts de l'association](/pdf/STATUTS-AXIOM-TEAM-032019.pdf)

## Vie privée

Ce site internet n'utilise aucun cookie, aucune base de donnée.

Vous pouvez vérifier et même héberger par vous même ce site internet : l'intégralité du code et de son contenu est ouvert et accessible sur [un dépôt git](https://git.duniter.org/documents/).

## Propriété intellectuelle

Associer ces deux termes [est un non-sens](https://www.gnu.org/philosophy/not-ipr.fr.html) ([cf. Discussion](https://www.april.org/utilisation-de-lexpression-propriete-intellectuelle-richard-stallman-albert-jacquard) avec Albert Jacquard). Cela dit, sauf mention contraire, tous les contenus sont sous [Licence CC BY SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr).
