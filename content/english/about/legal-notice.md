---
title: "Terms"
lang: en
#date: 2019-07-06T15:27:17+06:00
draft: false
bg_image: "images/backgrounds/puzzle.jpg"
description : "Legal Notice"
image: "images/logo.png"
categories: ["about"]
tags: ["governance"]
type: "post"
---



Page to write using markdown [on a pad](https://pad.p2p.legal/).

Or to submit directly [on our git repository](https://git.duniter.org/documents/librecurrency-rd-documentation/-/tree/master/content).

## Editor

[Axiom-Team status](/pdf/STATUTS-AXIOM-TEAM-032019.pdf)

## Privacy

This website does not use any cookie or database.

You can check and even host this website yourself: all the code and its content is open and accessible on [a git repository](https://git.duniter.org/documents/).

## Intellectual property

Combining these two terms [is nonsense](https://www.gnu.org/philosophy/not-ipr.en.html). That said, unless otherwise stated, all content is under the [CC BY SA 4.0 License](https://creativecommons.org/licenses/by-sa/4.0/).
