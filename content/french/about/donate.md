---
title: "Contribuer financièrement"
lang: fr
#date: 2019-07-06T15:27:17+06:00
draft: false
bg_image: "images/backgrounds/puzzle.jpg"
description : "Faites un don à Axiom-Team pour soutenir les monnaies libres."
image: "images/donate/logo_g1_flare_512.png"
categories: ["about"]
tags: ["founding"]
type: "post"
---


Vous pouvez contribuer par un don en June (Ǧ1)  sur le compte d’Axiom Team.
Clé publique :
[8CWuf4f1jYoVzHh4DEFpCyzZYC1pgz4t2wU8F2zKCthh](https://g1.duniter.fr/#/app/wot/8CWuf4f1jYoVzHh4DEFpCyzZYC1pgz4t2wU8F2zKCthh/)

Pour une  participation en euros  (don, adhésion, cagnotte, etc..) vous pouvez passer par notre [page d'adhésion](https://axiom-team.fr/adherer), ou en envoyant un chèque à : Axiom Team 14 rue Johannes Kepler 31500 Toulouse.


