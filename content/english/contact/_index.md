---
title: "Contact"
draft: false
# page title background image
bg_image: "images/backgrounds/world.jpg"
# meta description
description : ""
---

You can contact us in several ways:

### In oour chat rooms

https://chat.axiom-team.fr/

### On the forum

https://forum.monnaie-libre.fr/

### By email

