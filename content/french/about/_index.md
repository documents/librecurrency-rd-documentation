---
title: "À propos de nous"
draft: false
# page title background image
bg_image: "images/banner/banner-1.jpg"
# about image
image: ""
# meta description
description : ""
---

![pugs puppies](/images/logo.png)

### L'association Axiom-Team remplit deux missions :

* Aider les membres à promouvoir la monnaie libre  en fournissant aux utilisateurs des outils pour organiser des événements locaux. Nous proposons des créations graphiques (modèles de flyers, d’affiches, de stickers) et des conseils logistiques sous forme de documents  disponibles en ligne .

* Soutenir les développeurs de l'écosystème Duniter  en mettant à disposition des outils d'organisation et des financements. Pour cela, nous avons  deux cagnottes  permanentes destinées à accueillir les dons en euro et ğ1.

### Une équipe dévouée

Les membres d'Axiom Team ont associé leurs énergies pour produire une vision à la fois diverse et cohérente de la monnaie libre. En réunissant des profils d'électrons libres au service d'une Monnaie Libre, nous espérons diffuser progressivement l'idée d'un changement de paradigme monétaire en douceur, parmi toutes les communautés, être humain par être humain, librement.

### Des antennes Axiom Team

L'association fonctionne par antennes régionales de manière à décentraliser l'organisation des événements.

Chaque antenne régionale dispose de  la couverture d'assurance que lui fournit l'association Axiom-Team .
Elle dispose également du support et de l'entraide du réseau monnaie libre, et l'accompagnement dans les démarches administratives si il y en a.

Pour postuler en tant qu'antenne d'Axiom-Team, il vous suffit d'envoyer un mail à contact@axiom-team.fr
Prévoyez 2/3 jours de délais de réponse.

