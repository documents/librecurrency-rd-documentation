---
title: dev meeting report
description: 02/03/2021
date: 2021-03-02
draft: true
lang: fr
#bg_image: "images/backgrounds/library.jpg"
#image: "images/blog/"
categories: ["report"]
tags: ["blockchain", "gchange"]
type: "post"
---



## Fusion Pod Gchange/Cesium+ (gchange.fr readonly)

Ce projet nécessite avant tout de restreindre l’accès à gchange.fr en lecture seul.
Les G1Liens ont été évoqués, le protocole mis en place par @1000i100 (qui n’était pas présent à la réunion) ne semble pas réaliste, il faudrait tout revoir à ce niveau là pour faire une RFC réaliste.
L’idée d’un plugin navitageur G1Compagnon semble séduire pas mal de monde:
Il s’agirait d’inviter tous les junistes utilisant un outil G1 en ligne d’installer ce plugin firefox/chrome qui permettrait de faire le lien entre certaines actions de certains site (cesium web, gchange.fr, ect…) et l’outil ou le plugin navigateur correspondant, pour que toutes les parties authentifiés se déroulent naturellement via l’outil local.

## GVA, ajout des requêtes Heads et Peers pour le p2p

@vit va se mettre sur GVA pour ajouter les fonctions de Heads (branche majoritaire) et Peers (endpoints). Ceci afin de pouvoir faire de Tikka un client p2p dès le départ.

Tout le monde peut se mettre à Rust, c’est un langage pas plus compliqué qu’un autre ! Allons-y !

## Module Navigateur G1 Companion

Pour gérer les installations et le dialogues entre les sites et les applications, il faudrait un module de navigateur qui gère cela via des liens URI. @1000i100 @poka (voir section Fusion des pods Cs+/gchange)

## RFC Checksum

@matograine va refaire une RFC qui corrige le problème du 1 en début de clef. On laissera le double sha256 hérité de Bitcoin.
Problèmes de synchro Duniter

Le problème sera plus rapidement contourné par des clients p2p avant d’être corrigé dans Duniter.

## Problèmes de synchro Cesium+/Gchange

Le problème constaté par @fdrubigny est peut-être dû à la coupure fibre du serveur officiel, qui a durée deux semaines ! Les clients peuvent contourner le problème en étant p2p.
Transactions rapides entre client et commerçant

 * La technique des canaux est à envisager sérieusement (Lightning Networks).
 * Les copies de documents entre terminaux en NFC (cartes…) ou autre aussi. @kimamila

# DuniterPy n’est pas mort

DuniterPy est utile surtout pour ses modules de bibliothèque, plus que pour son client de connexion.
Il faut ajouter le mnemonic et le Dewif de Tikka sur la branche 1.0.

## Bibliothèque Rust pour tous les clients

Cette idée est très bonne dans le principe d’unifier les algorithmes de cryptographie pour les clients Duniter. Mais @vit y voit plein de défauts dans la pratique. Problèmes de packaging impliquant une bibliothèque compilée, bus factor sur un unique développeur déjà très occupé sur le cœur. Bien sûr si plus de développeurs garantissent la maintenance du projet et si des contributeurs se charge des packaging par plateforme, ce projet est tout à fait pertinent.

