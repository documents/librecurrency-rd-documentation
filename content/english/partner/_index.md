---
title: "Partners"
draft: false
# page title background image
bg_image: "images/backgrounds/world.jpg"
# meta description
description : "Companies, associations and administrations developing, promoting or using libre currencies. [Contact us](/contact) to become a partner!"
---
