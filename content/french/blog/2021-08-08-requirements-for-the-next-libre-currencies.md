---
title: Cahiers des charges pour les futures monnaies libres
description: IMPORTANT
date: 2021-08-09
draft: true
lang: fr
#bg_image: "images/backgrounds/library.jpg"
#image: "images/blog/"
#author: jbar
categories: ["study"]
tags: ["identity", "blockchain", "ux"]
type: "post"
---

## Synthèse de l'expérience "Ğ1"

Grâce à la June (Ğ1), nous avons pu expérimenter à petite échelle la viabilité
d'une "**monnaie libre**", c'est à dire d'un système monétaire totalement nouveau:
 * sans réserves fractionnaires (à l'opposé de la "**monnaie dette**")
 * à dividende (monétaire) universel (à l'opposé de la création monétaire à
   destination de "marchés" privilégiés) 

Cette expérience confirme les bienfaits de ce type de système:

Là où toutes les autres cryptomonnaies ne servent plus qu'à spéculer en **monnaie
dette**, la **monnaie libre** favorise les échanges locaux et le bien être commun.

Ceci dit nous avons aussi pu expérimenter certaines limites, principalement
techniques :
 * croissance de la sphère économique extrêmement lente (nombre de "junistes"
   évolue très doucement, avec des décroissances ponctuelles et des plateaux)
 * Expérience utilisateur (ux) compliquée et peu attractive
 * Problèmes de performances (surcharges et synchronisation des nœuds)
 * Problèmes de sécurités (Non Disclosed)
 * Problèmes de résilience (les junistes sont devenue dépendants de certains
   services qui, contrairement à la blockchain au cœur de la June, ne sont pas suffisamment décentralisés)
 * Problème de montée en charge. (Cf problèmes de performances)

Bref, contrairement aux attentes de beaucoup de "juniste", la June (Ğ1), n'est
pas prête de se substituer, même localement à l'euro.

D'autres relèvent aussi que cette expérience soulève certaines questions et
problèmes qui méritent réflexions et solutions:
 * souveraineté
 * démocratie
 * gestion des communs (impôt)
 * stabilité (l'expérience montre que la valeur qu'en l'absence de
   place/plateforme de change commune, les junistes prêtent à la
   June des valeurs très différentes, dans l'espace et dans le temp. Ce qui
   invalide en partie la théorie.)
 * transition (remplacement réflechi et calculé de la **monnaie dette**)
 * maintenance du système, gestion de ses coûts inhérents (exemple: infrastructures, ingénieurs... cf. gestion des communs)



## Cahier des charges pour les prochaines **monnaies libres**


### Ğ1

Le cahiers des charges de la June n'a hélas jamais été écrit.

Les exigences et roadmaps des prochaines **monnaies libres** ne peuvent être imposées,
et aucun "Junistes" ne sera forcé à changer de blockchain (et de protocole inhérent)


### "G2"

"G2" est un nom de code qui désigne aujourd'hui à la fois :
 * un **protocole** ou **format**. C'est à dire des spécifications techniques, indépendante de
  tout choix de langage de programmation ou de couche logicielle.
 * une première **implémentation** de ce protocole
 * une première **monnaie** suivant ce protocole. C'est à dire une première famille
   de jeton numérique et cryptographiquement infalsifiables.

#### Cahier des charges

1. Le protocole "G2" doit permettre le dividende universel :
    1. doit authentifier tous les humains de façon unique.
    2. doit permettre une création monétaire identique à chacun
    3. Le dividende universelle doit être possible pour tout humain, sans aucune
       limite que le fait d'être en vie (∀ { âge, géographie, capacité, ...} )
2. Les unités de "G1" devront être transposables dans la monnaie "G2".
    1. La majorité des utilisateurs de la "G1" devrait avoir un intérêt à
    privilégier la monnaie "G2".
    2. Le protocole "G2" doit permettre des expériences utilisateurs (ux) bien plus
    attractives que la "G1"
    3. La sécurité du protocole "G2" doit être bien supérieure à celui de la "G1"
3. Le nombre d'utilisateurs de la monnaie G2 doit pouvoir croître de façon
    exponentielle.
4. Le protocole "G2" doit pouvoir permettre de gérer et financer des communs (dans ses monnaies),
    à commencer par son implementation de référence.
5. Le protocole G2 doit pouvoir tenir l'épreuve du temps :
    1. doit s'appuyer sur d'autres protocoles eux-mêmes éprouvées
    2. L'implémentation de référence doit s'appuyer sur le minimum de dépendances et uniquement sur des dépendances éprouvées.
    3. Le protocole G2 doit inventer ou ré-inventer le minimum de chose
    4. L'implémentation de référence doit contenir le moins de lignes de code
       possible.

DRY KISS !

### G3

Une "G3" est encore très hypothétique, et ne devraient pas voir le jour avant
plusieurs années. Tout dépend de l'utilisation en pratique du protocole "G2".

Mais si une monnaie "G2" réussit le défi de satisfaire un nombre exponentiel
d'individu, et que l'ensemble de la population mondiale souhaite l'adopter.

Alors il faudra sans doute une "G3", basé sur une technologie supérieurs à
toutes les blockchains que nous connaissons aujourd'hui

### Feuille de route pour la prochaine monnaie libre "G2"

Nous en sommes encore à la recherche et l'étude de solutions sur lesquelles nous
pourrions nous appuyer.

Nous nous donnons jusqu'à fin Octobre pour finaliser les choix et démarrer en parallèle
des spécifications et une implémentation sérieuse.

Pour l'instant nous pensons que le protocole G2 pourrait s'appuyer sur :
 * le format OpenPGP pour gérer la toile de confiance et la communication entre
   les humains authentifiés dans une monnaie (Interopérabilité pour les utilisateurs, facilitant l'expérience utilisateur).
 * le schéma de clé asymétrique [ed25519](https://ed25519.cr.yp.to/index.html)
 * l'algorithme [scrypt](https://fr.wikipedia.org/wiki/Scrypt)

Et que sa première implémentation pourrait s'appuyer sur :

 * le framework [substrate](https://substrate.dev/) pour les nœuds

 * La couche OpenKeyChain pour les clients Android et IOS

...

